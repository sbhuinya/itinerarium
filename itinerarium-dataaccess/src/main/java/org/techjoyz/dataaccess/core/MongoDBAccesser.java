package org.techjoyz.dataaccess.core;

import com.mongodb.*;
import org.mongodb.morphia.Morphia;

import javax.inject.Inject;

/**
 * Abstract accessor for MongoDB implementations
 */
public abstract class MongoDBAccesser<T> implements CoreDataAccesser<T>{

    /** the db object providing hook to mongodb server */
    @Inject
    private DB mongoDB;

    /** The morphia object to map object */
    @Inject
    private Morphia morphia;

    public MongoDBAccesser(MongoClient mongoClient) {
        this.mongoDB = mongoClient.getDB("test");
    }

    public MongoDBAccesser() {

    }


    @Override
    public boolean insert(T data) {
        DBCollection collection = mongoDB.getCollection(getCollectionName());
        WriteResult result = collection.insert(morphia.toDBObject(data));
        return result.getN() > 0 ? true : false;
    }

    @Override
    public boolean update(T data) {
        DBCollection collection = mongoDB.getCollection(getCollectionName());
        DBObject objectToUpdate = morphia.toDBObject(data);
        DBObject query = new BasicDBObject("_id", new BasicDBObject("$eq", objectToUpdate.get("_id")));
        WriteResult result = collection.update(query, objectToUpdate, false, false);
        return result.getN() > 0 ? true : false;
    }


    /**
     * Returns the collection name applicable to the document
     * @return the collection name
     */
    protected abstract String getCollectionName();

    public void setMongoDB(DB mongoDB) {
        this.mongoDB = mongoDB;
    }

    public void setMorphia(Morphia morphia) {
        this.morphia = morphia;
    }
}
