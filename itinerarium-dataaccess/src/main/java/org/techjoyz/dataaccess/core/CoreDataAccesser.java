package org.techjoyz.dataaccess.core;

/**
 * Interface for providing access to the data
 */
public interface CoreDataAccesser<T> {

    /**
     * Inserts the data provided in the DB
     * @param data teh data provided
     * @return true is success else false
     */
    boolean insert(T data);

    /**
     * Updates the data in the database
     * @param data the data to be updated
     * @return true if success else false
     */
    boolean update(T data);
}
