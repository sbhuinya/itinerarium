package org.techjoyz.itinerarium.dataaccess.qualifier;

import javax.inject.Qualifier;
import java.lang.annotation.*;

/**
 * Created by surajeet on 19/08/14.
 */
@Qualifier
@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD })
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface UserProfileAccessor {
}
