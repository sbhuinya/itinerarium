package org.techjoyz.itinerarium.dataaccess.dao;

import com.mongodb.MongoClient;
import org.techjoyz.dataaccess.core.MongoDBAccesser;
import org.techjoyz.itinerarium.core.model.UserProfile;

/**
 * Created by surajeet on 25/07/14.
 */
public class UserProfileAccesser extends MongoDBAccesser<UserProfile> {

    public UserProfileAccesser(MongoClient mongoClient) {
        super(mongoClient);
    }

    public UserProfileAccesser() {
        super();

    }

    @Override
    protected String getCollectionName() {
        return "userprofiles";
    }
}
