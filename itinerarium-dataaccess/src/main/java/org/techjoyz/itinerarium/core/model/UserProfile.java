package org.techjoyz.itinerarium.core.model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Created by surajeet on 25/07/14.
 * Data model for user profile
 */
@Entity(noClassnameStored = true)
public class UserProfile {
    @Id
    private ObjectId id;

    private String firstName;

    private String lastName;

    private String emailId;

    private String gender;

    private String username;

    private String password;


    @Embedded
    private UserTravelPreference userTravelPreference;

    public UserTravelPreference getUserTravelPreference() {
        return userTravelPreference;
    }

    public void setUserTravelPreference(UserTravelPreference userTravelPreference) {
        this.userTravelPreference = userTravelPreference;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public ObjectId getId() {
        return id;
    }


    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
