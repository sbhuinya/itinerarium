package org.techjoyz.itinerarium.core.model;

import org.mongodb.morphia.annotations.Embedded;

import java.util.List;

/**
 * Created by surajeet on 25/07/14.
 *
 * User's travel preference
 */
@Embedded
public class UserTravelPreference {

    private List<String> interests;

    private int travelIntensity;


    public int getTravelIntensity() {
        return travelIntensity;
    }

    public void setTravelIntensity(int travelIntensity) {
        this.travelIntensity = travelIntensity;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }
}
