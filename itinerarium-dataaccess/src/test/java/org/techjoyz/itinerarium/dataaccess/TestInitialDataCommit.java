package org.techjoyz.itinerarium.dataaccess;

import com.mongodb.*;
import org.mongodb.morphia.Morphia;
import org.techjoyz.itinerarium.core.model.UserProfile;
import org.techjoyz.itinerarium.core.model.UserTravelPreference;
import org.techjoyz.itinerarium.dataaccess.dao.UserProfileAccesser;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by surajeet on 27/07/14.
 */
public class TestInitialDataCommit {

    public static void main(String[] args) throws UnknownHostException {
// or
// or
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        DB db = mongoClient.getDB( "test" );

        if (db.collectionExists("testData")) {
            DBCursor cursor = db.getCollection("testData").find();
            for (DBObject data : cursor.toArray()) {
                //System.out.println(data);
            }
        }

        Morphia morphia = new Morphia();
        morphia.map(UserProfile.class);

        UserProfileAccesser upa = new UserProfileAccesser();
        upa.setMongoDB(db);
        upa.setMorphia(morphia);

        UserTravelPreference utp = new UserTravelPreference();
        utp.setInterests(Collections.singletonList("carzy"));
        utp.setTravelIntensity(2);

        UserProfile up = new UserProfile();
        up.setFirstName("Surajeet");
        up.setLastName("Bhuinya");
        up.setEmailId("sbhuinya@gmail.com");
        up.setGender("Male");
        up.setUsername("suro");
        up.setPassword("suro");
        up.setUserTravelPreference(utp);

        //upa.insert(up);
        DBCollection dbCollection = db.getCollection("userprofiles");
        List<DBObject> allData = dbCollection.find().toArray();
        if (dbCollection != null) {

            for (DBObject data : allData) {
                UserProfile upnew = morphia.fromDBObject(UserProfile.class, data);
            upnew.getUserTravelPreference().setInterests(Arrays.asList("crazy"));
            //upa.update(upnew);
            }
        }
        DBObject firstData = allData.get(0);
        UserProfile upnew = morphia.fromDBObject(UserProfile.class, firstData);
        upnew.getUserTravelPreference().getInterests().add("museum");
        upa.update(upnew);
        DBObject query = new BasicDBObject("userTravelPreference.interests", new BasicDBObject("$in",
                Arrays.asList("museum")));
        DBCursor cursor = dbCollection.find(query);
        for (DBObject data : cursor.toArray()) {
            System.out.println("::::::::"+data);
        }

    }
}
