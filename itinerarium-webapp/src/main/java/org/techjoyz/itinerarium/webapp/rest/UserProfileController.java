package org.techjoyz.itinerarium.webapp.rest;

import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import org.mongodb.morphia.Morphia;
import org.techjoyz.itinerarium.core.model.UserProfile;
import org.techjoyz.itinerarium.core.model.UserTravelPreference;
import org.techjoyz.itinerarium.dataaccess.dao.UserProfileAccesser;
import org.techjoyz.itinerarium.dataaccess.qualifier.UserProfileAccessor;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.FileInputStream;
import java.util.Collections;

/**
 * Created by surajeet on 10/08/14.
 */
@Path("/users")
public class UserProfileController {

    @Inject
    MongoClient mongoClient;

    @Inject @UserProfileAccessor
    UserProfileAccesser upa;

    @Inject
    Morphia morphia;

    @GET
    @Path("/home")
    @Produces(MediaType.TEXT_HTML)
    public Response showSimplePage() {
        FileInputStream fis = (FileInputStream) this.getClass().getClassLoader().getResourceAsStream("/views/home.html");
        return Response.ok(fis).build();
    }
}
