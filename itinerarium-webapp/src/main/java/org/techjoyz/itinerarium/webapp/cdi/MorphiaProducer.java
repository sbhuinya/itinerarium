package org.techjoyz.itinerarium.webapp.cdi;

import com.mongodb.MongoClient;
import org.mongodb.morphia.Morphia;
import org.techjoyz.itinerarium.dataaccess.dao.UserProfileAccesser;
import org.techjoyz.itinerarium.dataaccess.qualifier.UserProfileAccessor;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

/**
 * Created by surajeet on 21/08/14.
 */
public class MorphiaProducer {

    @Produces
    @ApplicationScoped
    public Morphia getMorphia() throws Exception {

        return new Morphia();
    }

    @Produces
    @ApplicationScoped
    @UserProfileAccessor
    public UserProfileAccesser getUserDao(@Named("mongoClient")MongoClient mongoClient, Morphia morphia) throws Exception {
        UserProfileAccesser result = new UserProfileAccesser(mongoClient);
        result.setMorphia(morphia);
        return result;
    }
}
