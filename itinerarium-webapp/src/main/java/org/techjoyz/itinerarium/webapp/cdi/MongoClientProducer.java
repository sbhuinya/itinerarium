package org.techjoyz.itinerarium.webapp.cdi;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import java.net.UnknownHostException;

/**
 * Mongo DB data source producer
 */
public class MongoClientProducer {

    /** system property key for Mongo db location */
    private static final String MONGO_DB_URI = "itinerarium.mongodb.uri";

    @Produces  @ApplicationScoped @Named("mongoClient")
    public MongoClient getMongoClient() throws Exception {
        String mongoUri = System.getProperty(MONGO_DB_URI);
        if (mongoUri == null) {
            throw new IllegalArgumentException("Cannot start application with no DB support");
        }
        MongoClientURI mongoClientURI = new MongoClientURI(mongoUri.toString());
        MongoClient mongoClient = null;
        try {
            mongoClient = new MongoClient( mongoClientURI);
        } catch (UnknownHostException e) {
            //TODO exception handling for the application
            e.printStackTrace();
        }

        return mongoClient;
    }


}
