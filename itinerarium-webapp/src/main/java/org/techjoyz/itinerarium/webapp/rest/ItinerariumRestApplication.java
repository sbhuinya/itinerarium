package org.techjoyz.itinerarium.webapp.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by surajeet on 11/08/14.
 */
@ApplicationPath("/itinerarium")
public class ItinerariumRestApplication extends Application {
}
